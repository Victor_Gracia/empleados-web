package profe.empleados.empleadosweb.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import profe.empleados.empleadosweb.services.EmpleadosService;
import profe.empleados.exceptions.EmpleadoNoEncontradoException;
import profe.empleados.exceptions.EmpleadoYaExisteException;
import profe.empleados.exceptions.EmpleadosOpNoAutorizadaException;
import profe.empleados.model.Empleado;

@Controller
@RequestMapping("/gestEmpleados")
public class EmpleadosWebController {

	private static final Logger logger = LoggerFactory.getLogger(EmpleadosWebController.class);
	
	@Autowired
	private EmpleadosService empleadosService;

	@ModelAttribute("empleadoAMostrar")
	public Empleado empleado() {
		return new Empleado();
	}
	
	@GetMapping
	public String muestraFormulario() {
		logger.info("Llego a muestra formulario");
		return "form-empleados";
	}

	@PostMapping(params = {"listar"})
	public String muestraEmpleados(Model model) {
		model.addAttribute("listaEmpleados", empleadosService.getAllEmpleados());
		return "form-empleados";
	}

	@PostMapping(params = {"getOne"})
	public String muestraUnEmpleado(@RequestParam String cif, Model model) {
		model.addAttribute("empleadoAMostrar", empleadosService.getEmpleado(cif));
		return "form-empleados";
	}

	@PostMapping(params = {"eliminar"})
	public String eliminaEmpleado(@RequestParam String cif, Model model) {
		empleadosService.eliminaEmpleado(cif);
		model.addAttribute("mensaje", "Empleado con cif " + cif + " eliminado correctamente");
		return "form-empleados";
	}

	@PostMapping(params = {"insertar"})
	public String insertaEmpleado(@ModelAttribute Empleado emp, Model model) {
		empleadosService.insertaEmpleado(emp);
		model.addAttribute("mensaje", "Empleado con cif " + emp.getCif() + " insertado correctamente");
		return "form-empleados";
	}

	@PostMapping(params = {"modificar"})
	public String modificaEmpleado(@ModelAttribute Empleado emp, Model model) {
		empleadosService.modificaEmpleado(emp);
		model.addAttribute("mensaje", "Empleado con cif " + emp.getCif() + " modificado correctamente");
		return "form-empleados";
	}

	@ExceptionHandler(EmpleadoNoEncontradoException.class)
	public String handleEmpleadoNoEncontradoException(Model model) {
		model.addAttribute("mensaje", "Error, el empleado sobre el que intentas operar no existe");
		model.addAttribute("empleadoAMostrar", new Empleado());
		return "form-empleados";
	}

	@ExceptionHandler(EmpleadoYaExisteException.class)
	public String handleEmpleadoYaExisteException(Model model) {
		model.addAttribute("mensaje", "Error, ese empleado ya existe");
		model.addAttribute("empleadoAMostrar", new Empleado());
		return "form-empleados";
	}

	@ExceptionHandler(EmpleadosOpNoAutorizadaException.class)
	public String handleEmpleadosOpNoAutorizadaException(Model model) {
		model.addAttribute("mensaje", "Error, no tiene permisos para ejecutar esta operación");
		model.addAttribute("empleadoAMostrar", new Empleado());
		return "form-empleados";
	}
	
	@ExceptionHandler(Exception.class)
	public String handleException(Model model, Exception e) {
		logger.error("Error:", e);
		model.addAttribute("mensaje", "Error al ejecutar la operación, inténtelo de nuevo en unos segundos");
		model.addAttribute("empleadoAMostrar", new Empleado());
		return "form-empleados";
	}
	
	
}
